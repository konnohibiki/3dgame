using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadGenerator : MonoBehaviour
{
    public GameObject BadBall;
    float span = 1.0f;
    float delta = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.delta += Time.deltaTime;
        if (this.delta > this.span)                                          //deltaが2個以上のとき
        {
            this.delta = 0;
            GameObject go = Instantiate(BadBall) as GameObject;
            int px = Random.Range(-6, 7);                                   //矢がランダムで出る範囲を指定
            go.transform.position = new Vector3(20, 2, px);
        }
    }
}
