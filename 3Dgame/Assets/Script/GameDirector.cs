using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    public GameObject Score;
    int count = 0;
    GameObject hpGauge;
    int HP = 10;
    public static int score = 0;


    public static int getscore()
    {
        return score;
    }

    // Start is called before the first frame update
    void Start()
    {
        //「HPゲージ」のオブジェクトを探し、hpGauge変数に代入
        this.hpGauge = GameObject.Find("hpGauge");
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ScoreUp()
    {
        //スコアが足される
        count++;
        Score.GetComponent<Text>().text = "score:" + count;
    }
    public void DecreaseHP()
    {
        //HPゲージが1減る
        hpGauge.GetComponent<Image>().fillAmount -= 0.1f;
        //Debug.Log("oooooo");

        HP -= 1;

        //Debug.Log(HP);

        //もしHPが0になったら
        if (HP == 0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }
}
